
# Spring Prometheus Tutorial

Throughout this tutorial, we're going to learn about Prometheus and create a simple webapp with Spring Boot that will expose metrics. Then, we'll run a small Prometheus server and create a dashboard in Grafana! Let's get going!

## Background

To get a background of Prometheus, you can refer to the Google Slide presentation below. Additional links and resources are also available.

- [Google Slides presentation](https://docs.google.com/presentation/d/1l2kbNOPrQS49BvxFfzx6d_iPJiGJB4LPEQRZZPgLKNM/edit?usp=sharing)
- [Prometheus docs](https://prometheus.io/docs/introduction/overview/)


## Creating our Spring Boot App

In this section, we're going to build a simple Spring webapp that exposes a REST endpoint that provides simple greetings. Our endpoint will support an optional query parameter to change the name used in the greeting. We'll also add a hard-coded "invalid" name that, when used, will produce a 400. This way, we can try out creating graphs with error codes. 

### Getting a bootstrapped app

1. Go to [the Spring Initializr app](https://start.spring.io/).
1. At the time of writing this, the following settings were selected:
    - **Project:** Maven
    - **Language:** Java
    - **Spring Boot:** 2.3.3
    - **Packaging:** Jar
    - **Java:** 11
    - You can leave the defaults for the group, artifact, name, description, and package name
1. Add the following dependencies:
    - Spring Web
    - Spring Boot Actuator
1. Click the "Generate" button. A zip file should be downloaded.
1. Unzip the archive and open the project in your favorite IDE (you can get away with just a text editor for this tutorial though).


### Creating our REST endpoint

As mentioned earlier, we're going to create a simple endpoint that provides greetings.

1. In the `src/main/java/com/example/demo` directory, create a file named `GreetingController.java`. The file should have the following contents:

    ```java
    package com.example.demo;

    import org.springframework.http.HttpStatus;
    import org.springframework.stereotype.Controller;
    import org.springframework.web.bind.annotation.GetMapping;
    import org.springframework.web.bind.annotation.RequestMapping;
    import org.springframework.web.bind.annotation.RequestParam;
    import org.springframework.web.bind.annotation.ResponseBody;
    import org.springframework.web.server.ResponseStatusException;

    import java.util.Collections;
    import java.util.Map;

    @Controller
    @RequestMapping("/greetings")
    public class GreetingController {

        public static final String INVALID_NAME = "unknown";

        @GetMapping
        public @ResponseBody Map<String, String> getGreeting(
                @RequestParam(defaultValue = "world") String name
        ) {
            if (INVALID_NAME.equals(name)) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid name");
            }

            return Collections.singletonMap("greeting", "Hello " + name + "!");
        }
    }

    ```

1. Try running your project by running `mvn spring-boot:run`. It may take a few moments to download all of the dependencies, but you should eventually see a log message that looks like the following. If you don't have Maven installed, no worries! We'll setup a container to do it shortly.

    ```
    2020-08-19 21:01:39.225  INFO 3351 --- [nio-8080-exec-1] o.s.web.servlet.DispatcherServlet        : Initializing Servlet 'dispatcherServlet'
    2020-08-19 21:01:39.229  INFO 3351 --- [nio-8080-exec-1] o.s.web.servlet.DispatcherServlet        : Completed initialization in 4 ms
    ```

1. If it's running, open to [http://localhost:8080/greetings](http://localhost:8080/greetings) and you should see a JSON response like this:

    ```json
    {
        "greeting": "Hello world!"
    }
    ```


## Adding Prometheus Metric Support

When we added the "Spring Boot Actuator" dependency, that added support to expose internal metrics in many formats. To get Prometheus support, we only need to do a few minor things.

1. In the `src/main/resources/application.properties` file, add the following:

    ```
    management.endpoints.web.exposure.include=prometheus
    ```

1. In the `pom.xml` file, we need to add a depedency for Micrometer and it's Prometheus support. Spring will automatically detect the classes and start exposing internal metrics in a Prometheus-compatible format.

    Add the following into the `<dependencies>` tag in the `pom.xml`.

    ```xml
    <dependency>
        <groupId>io.micrometer</groupId>
        <artifactId>micrometer-registry-prometheus</artifactId>
    </dependency>
    ```

1. Restart the project by running `mvn spring-boot:run`. Now, if you open to [http://localhost:8080/actuator/prometheus](http://localhost:8080/actuator/prometheus), you'll see a bunch of default metrics being exposed! Note that this is NOT at the default Prometheus endpoint of `/metrics`. We'll have to configure Prometheus to pull from this URL later on.


## Creating a Docker Image

In order to run our application in a container, we'll need to create a Dockerfile. We'll start fairly simple, but then also get into a more advanced build that reduces build times and increases layer reuse.

### A quick and dirty build

1. At the root of the project, create a Dockerfile with the following contents:

    ```dockerfile
    FROM maven:3-jdk-11 AS build
    WORKDIR /app
    COPY . .
    RUN mvn clean package

    FROM openjdk:11-jre
    WORKDIR /app
    COPY --from=build /app/target/*.jar ./app.jar
    CMD java -jar app.jar
    ```

    This build is using a [multi-stage build](https://docs.docker.com/develop/develop-images/multistage-build). This allows us to use a `maven` image to do the Java compiling and building, but then use a much smaller and lighter JRE image for the final image. In production, we don't need a full JDK and Maven toolset, so why ship that with our image?

1. Let's build our image using the `docker build` command!

    ```
    docker build -t spring-prometheus .
    ```

1. Now, you can run your container!

    ```
    docker run --rm -tip 8080:8080 spring-prometheus
    ```

    Open your browser to [http://localhost:8080/greetings](http://localhost:8080/greetings) and you should see our greeting endpoint!


### Using thin jars

If you were to rebuild the image, you'll notice that only the last layer changes (since it is the jar). If we were to open the jar, we'd see _all_ of the dependencies needed to run the app. For larger apps, this could make the jar quite large. This is referred to as a **fat jar**. 

To maximize reuse and reduce the amount we have to push on each build, Spring supports the concept of a **thin jar**. Thin jars separate the dependencies from the app code. With thin jars, we could copy the dependencies into our container image in one layer and the app in a separate layer. That way, we reuse the dependencies until they change!

1. In the `pom.xml` file, located the `<plugin>` config for the `spring-boot-maven-plugin` plugin. Under the `<artifactId>` declaration, add the following. Spring will see this dependency and automatically enable the creation of the thin jar.

    ```xml
    <dependencies>
        <dependency>
            <groupId>org.springframework.boot.experimental</groupId>
            <artifactId>spring-boot-thin-layout</artifactId>
            <version>1.0.11.RELEASE</version>
        </dependency>
    </dependencies>
    ```

1. In the `pom.xml` file, we need to add one more plugin to the `<plugins>` configuration. This plugin creates the dependency file structure needed for Spring to start the application.

    ```xml
    <plugin>
        <groupId>org.springframework.boot.experimental</groupId>
        <artifactId>spring-boot-thin-maven-plugin</artifactId>
        <executions>
            <execution>
                <id>resolve</id>
                <goals>
                    <goal>resolve</goal>
                </goals>
                <inherited>false</inherited>
            </execution>
        </executions>
    </plugin>
    ```

1. We need to update our `Dockerfile` now to have two `COPY` instructions. Replace the existing `COPY` command with the following:

    ```dockerfile
    COPY --from=build /app/target/thin/root/repository ./repository
    COPY --from=build /app/target/thin/root/*.jar ./app.jar
    ```

    The first copy command is copying in all of the dependencies needed for the app (which is created by the second plugin) and the second is copying in the app itself.

1. At this point, go ahead and do a build.

    ```
    docker build -t spring-prometheus .
    ```

1. Now, go into the `GreetingController.java` file and make the greeting more exciting by adding more exclamation points. 

1. Do another build.

    ```
    docker build -t spring-prometheus .
    ```

1. If we look at this image history, we'll see that the layer containing the dependencies is being reused! Rebuilds only requiring a 11.3kB layer push certainly isn't too shabby!

    ```
    > docker image history spring-prometheus             
    IMAGE               CREATED             CREATED BY                                      SIZE
    64d20c5aef02        36 seconds ago      CMD ["/bin/sh" "-c" "java -jar app.jar --thi…   0B
    <missing>           36 seconds ago      COPY /app/target/thin/root/*.jar ./app.jar #…   11.3kB
    <missing>           12 minutes ago      COPY /app/target/thin/root/repository ./repo…   21.2MB
    <missing>           12 minutes ago      WORKDIR /app                                    0B
    <missing>           2 weeks ago         /bin/sh -c set -eux;   arch="$(dpkg --print-…   125MB
    <missing>           2 weeks ago         /bin/sh -c #(nop)  ENV JAVA_VERSION=11.0.8      0B
    ...
    ```


### (Optional) Using BuildKit's Features

With the latest build engine, called BuildKit, custom "syntaxes" can be supported. At the end of the day, Docker leverages another binary to "translate" the Dockerfile into the commands that need to run to produce the build. This allows for many different types of files and build use cases.

For our use case, we're going to use an "official" syntax that provides the ability to mount volumes during a build. This will let us download the Spring dependencies once and reuse them for subsequent builds. Faster builds FTW!

1. To use a new syntax parser, we need to add a comment at the top of the Dockerfile to specify the source of the parser. This is actually a reference to a Docker image!

    ```
    # syntax=docker/dockerfile:experimental
    ```

1. With that in place, we can add a mount to our `RUN` command (which is when our dependencies were being downloaded). For more mount types and options, you can [view the syntax docs here](https://github.com/moby/buildkit/blob/master/frontend/dockerfile/docs/experimental.md). Replace the `RUN mvn clean package` command with the following:

    ```dockerfile
    RUN --mount=type=cache,target=/root/.m2 mvn clean package
    ```

1. Now, do a build! We first need to enable the BuildKit engine by setting the `DOCKER_BUILDKIT` environment variable. You'll see the dependencies get downloaded again, but subsequent builds shouldn't! Feel free to do a second build and see how much faster it is!

    ```
    export DOCKER_BUILDKIT=1
    docker build -t spring-prometheus .
    ```

    **Pro tip:** Many folks have been using BuildKit as their default builder for years now with zero impact. Feel free to add the setting of the `DOCKER_BUILDKIT` variable into your shell's profile.



## Setting up Prometheus and Grafana

Now that we have an app, let's spin up Prometheus and Grafana to create a dashboard! We're not going to spend a lot of time diving into how to deploy and manage the components, so will simply deploy them using with Docker Compose. We will have the smart reverse proxy (to get a signed cert too), our Spring app, Grafana, and Prometheus.

```mermaid
graph TD
    proxy[Devcom Proxy] --> Grafana
    proxy --> Prometheus
    proxy --> app
    Prometheus -- Scrapes from --> app[Spring App]
    Grafana -- Queries data from --> Prometheus
```

### Deploying our container stack

1. Prometheus needs some configuration to know where it will be scraping metrics from. In this scenario, we're going to use a static file for configuration. In the root of your Java project, create a file named `prometheus.yml` with the following content:

    ```yml
    global:
    scrape_interval: 15s

    scrape_configs:
    - job_name: 'application'
        metrics_path: '/actuator/prometheus'
        static_configs:
        - targets: ['app:8080'] # This is using the internal container-to-container name
    ```

    Note that the path we configured is at `/actuator/prometheus` (NOT the default Prometheus endpoint of `/metrics`). Prometheus will also scrape the metrics from a hostname of `app`, which will be utilizing container-to-container communication.

1. Create a `docker-compose.yml` file with the following contents:

    ```yaml
    version: "3.8"

    services:
        proxy:
            image: dtr.it.vt.edu/devcom/devcom-localhost-proxy:traefik-1.7
            ports:
                - 80:80
                - 443:443
            volumes:
                - /var/run/docker.sock:/var/run/docker.sock

        app:
            image: spring-prometheus
            labels:
                traefik.backend: app
                traefik.port: 8080
                traefik.frontend.rule: Host:app.localhost.devcom.vt.edu

        prometheus:
            image: prom/prometheus
            volumes:
                - ./prometheus.yml:/etc/prometheus/prometheus.yml
                - prometheus-data:/prometheus
            labels:
                traefik.backend: prometheus
                traefik.port: 9090
                traefik.frontend.rule: Host:prometheus.localhost.devcom.vt.edu

        grafana:
            image: grafana/grafana
            volumes:
                - grafana-data:/var/lib/grafana
            labels:
                traefik.backend: grafana
                traefik.port: 3000
                traefik.frontend.rule: Host:grafana.localhost.devcom.vt.edu

    volumes:
        prometheus-data: {}
        grafana-data: {}
    ```

1. Start up the compose stack!

    ```bash
    docker-compose up
    ```

### Checking out Prometheus

In most cases, you _won't_ be accessing Prometheus directly. But, it is helpful to have access, as you can validate it's querying metrics correctly.

1. Open the Prometheus dashboard by going to [https://prometheus.localhost.devcom.vt.edu](https://prometheus.localhost.devcom.vt.edu). Once you're there, go to the "Status" -> "Targets" page. You should eventually see the app with a state of "UP". This means Prometheus is successfully scraping metrics.

    ![Screenshot of Prometheus page displaying targets](images/prometheus-target.png)

1. Go to the "Graph" page and let's do a simple query to look at the number of HTTP requests our app has handled. In the query field, enter `http_server_requests_seconds_count` and hit **Execute**. You should see a few results in the table. Switching to the "Graph" tab will display the results on a graph.

    ![Screenshot of Prometheus query](images/prometheus-results.png)


### Creating a Grafana dashboard

1. Open the Grafana app by going to [https://grafana.localhost.devcom.vt.edu](https://grafana.localhost.devcom.vt.edu).

1. Login using the default username of `admin` and password of `admin`. You'll be required to change the password, but can "change" it right back to `admin`! :laughing:

1. Once we're in Grafana, we need to configure a "data source". Click on the "Add your first data source" button on the home page.

1. Select **Prometheus** as the data source type.

1. Configure your data source with the following settings:
    - **Name:** You can leave it as the default or change it. But, go ahead and toggle the "Default" toggle so this data source will be our default source.
    - **URL:** http://prometheus:9090 (this will be using container-to-container communication)

1. Click **Save & Test** at the bottom. You should get a "Data source is working" message.

1. For our first dashboard, we're going to import a dashboard. Click on the "+" in the left-hand nav and click "Import".

1. In the "Import via grafana.com" section, enter the id `12464`.

1. Leave all of the default settings, but select your data source in the "Prometheus" field. Click the **Import** button.

1. Now that your dashboard is imported, feel free to open some of the collapsed rows and see additional panels. A few panels may not display data correctly, as metric names might have adjusted. See if you can fix some of them by editing the panels.

1. If you wish, create your own dashboard and create your own panels to display your own data.


## Adding our own Metric

While we have many default metrics, there are times in which we'd like to expose our own metrics. Let's add code to add our own custom metric to our app!

To do this, we'll move the creation of the greeting into a `GreetingService` class. Each time the service is invoked, we'll increment a counter.

1. In the `src/main/java/com/example/demo` folder, create a file named `GreetingService.java` with the following contents:

    ```java
    package com.example.demo;

    import io.micrometer.core.instrument.Counter;
    import io.micrometer.core.instrument.MeterRegistry;
    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.stereotype.Service;

    import javax.annotation.PostConstruct;

    @Service
    public class GreetingService {

        @Autowired
        private MeterRegistry meterRegistry;

        private Counter counter;

        @PostConstruct
        public void postConstruct() {
            this.counter = this.meterRegistry.counter("app_greetings_created");
        }

        public String getGreeting(String name) {
            this.counter.increment();
            return "Hello " + name + "!";
        }
    }
    ```

    This service is injected with the `MeterRegistry`, which is where all of the metric data is stored. Once the service is created, the `postConstruct` method is invoked to create a counter. Each time `getGreeting` is invoked, we'll increment the counter!

1. Now we just need to update our `GreetingController` to use the new service. Under the `public static final...`, add the following:

    ```java
    @Autowired
    private GreetingService greetingService;
    ```

1. And in our endpoint method, update our `return` statement to have the following:

    ```java
    return Collections.singletonMap("greeting", greetingService.getGreeting(name));
    ```

1. Build the app and run `docker-compose up`. You should then see a new metric!

## Add the Metric to a Grafana Dashboard

1. Back in Grafana ([https://grafana.localhost.devcom.vt.edu](https://grafana.localhost.devcom.vt.edu)), open either your existing dashboard or make a new one.

1. Click on the "+" to create a new panel.

1. In the Metrics field, enter `app_greetings_created_total`. You should then see a chart! Feel free to play with some of the other [Prometheus functions](https://prometheus.io/docs/prometheus/latest/querying/functions/) to display a rate of greetings, etc.


## Wrapping Up

With this tutorial, we created a new Spring Boot app and configured it to expose default metrics. We then deployed a local instance of Grafana and Prometheus to practice creating a dashboard. We then even added our own metric to be exposed, allowing us to create our own app-specific dashboards.
